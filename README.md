*OUTDATED* Please note that i will not update this repository any more.

# Font No Underscore
*Droid Sans Mono* font with underscores replaced by a whitespace sign, half width.

[Download][down]

See the [wiki] for screenshots.

Author: [Nils-Hero Lindemann]. Contact me if you want your preferred free font to be included.

[down]: https://bitbucket.org/nilshero/font-no-underscore/downloads/Droid_Sans_Mono_NU.ttf "Download Droid_Sans_Mono_NU.ttf"
[Nils-Hero Lindemann]: mailto:nilsherolindemann@gmail.com "nilsherolindemann@gmail.com"
[wiki]: https://bitbucket.org/nilshero/font-no-underscore/wiki/Home "Font No Underscore -> wiki"
